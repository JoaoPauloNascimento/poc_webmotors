#language: pt

@poc_api
Funcionalidade: Validar POC WebMotors

  @make
  Esquema do Cenário: Consulta servico make sem filtro por marca
    Dado que eu consulte o contrato make de marcas
    Entao deve retorna com o valor <marca> disponivel
    Exemplos:
      |codigo_esperado|marca      |
      |'200'          |'Chevrolet'|
      |'200'          |'Honda'    |
      |'200'          |'Ford'     |

  @model
  Esquema do Cenário: Consulta servico model
    Dado que eu consulte o contrato make de marcas
    Quando consultar o contrato model com a marca <marca>
    Entao deve retorna com o valor <modelo> disponivel
    Exemplos:
      |codigo_esperado|marca      |modelo    |
      |'200'          |'Chevrolet'|'Agile'   |
      |'200'          |'Chevrolet'|'Astra'   |
      |'200'          |'Chevrolet'|'Onix'    |
      |'200'          |'Honda'    |'City'    |
      |'200'          |'Honda'    |'CRV'     |
      |'200'          |'Honda'    |'Fit'     |
      |'200'          |'Ford'     |'Ecosport'|
      |'200'          |'Ford'     |'Fussion' |

  @version
  Esquema do Cenário: Consulta servico version
    Dado que eu consulte o contrato make de marcas
    Quando consultar o contrato model com a marca <marca>
    E consulta as versoes disponiveis do modelo <modelo>
    Entao deve retorna com o valor <versao> disponivel
    Exemplos:
      |codigo_esperado|marca      |modelo    |versao                         |
      |'200'          |'Chevrolet'|'Agile'   |'1.5 DX 16V FLEX 4P AUTOMÁTICO'|
      |'200'          |'Chevrolet'|'Astra'   |'1.5 DX 16V FLEX 4P AUTOMÁTICO'|
      |'200'          |'Chevrolet'|'Onix'    |'1.5 DX 16V FLEX 4P AUTOMÁTICO'|
      |'200'          |'Honda'    |'City'    |'1.0 MPI EL 8V FLEX 4P MANUAL' |
      |'200'          |'Honda'    |'CRV'     |'1.5 LX 16V FLEX 4P MANUAL'    |
      |'200'          |'Honda'    |'Fit'     |'1.5 LX 16V FLEX 4P MANUAL'    |
      |'200'          |'Ford'     |'Ecosport'|'1.5 DX 16V FLEX 4P AUTOMÁTICO'|
      |'200'          |'Ford'     |'Fussion' |'1.5 DX 16V FLEX 4P AUTOMÁTICO'|