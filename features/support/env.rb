# frozen_string_literal: true

require 'cucumber'
require 'httparty'
require 'report_builder'
require 'rspec'
require 'rubocop'
require 'yaml'
require 'pry'
require_relative 'pagina_objetos'
require_relative 'carregar_arquivo'

World(PaginaObjetos)
World(CarregarArquivo)

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8